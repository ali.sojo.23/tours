import { Component, OnInit } from "@angular/core";

@Component({
  selector: "app-calendar",
  templateUrl: "./calendar.component.html",
  styleUrls: ["./calendar.component.css"]
})
export class CalendarComponent implements OnInit {
  constructor() {}
  mes = [
    "Enero",
    "Febrero",
    "Marzo",
    "Abril",
    "Mayo",
    "Junio",
    "Julio",
    "Agosto",
    "Septiembre",
    "Octubre",
    "Noviembre",
    "Diciembre"
  ];
  year: number;
  year2;
  month: number;
  month2;
  days0;
  week0;
  days;
  week;
  days2;
  week2;

  date1;
  date2;
  ngOnInit() {
    this.date();
  }

  date() {
    //Declarar nueva fecha
    let fecha = new Date(Date.now());
    //Obtener la fecha actual
    this.year = fecha.getFullYear();
    //Obtener mes actual
    this.month = fecha.getMonth();

    //Obtener dias del mes
    this.days = new Date(this.year, this.month + 1, 0).getDate();

    this.week = new Date(this.year, this.month, 1).getDay();

    // Obtener el mes siguiente
    this.nextMonth(this.year, this.month);
  }
  nextMonth(año, month) {
    let mes = month + 1;
    if (mes < 12) {
      this.month2 = mes;
      this.days2 = new Date(año, this.month2 + 1, 0).getDate();
      this.week2 = new Date(año, this.month2, 1).getDay();
    } else {
      this.month2 = 0;
      console.log(this.month2);
      this.year2 = año + 1;
      this.days2 = new Date(this.year2, 0, 0).getDate();
    }
  }

  calendary(number) {
    let items: number[] = [];
    for (var i = 1; i <= number; i++) {
      items.push(i);
    }
    return items;
  }
  obtenerFecha(dia, mes, año) {
    let fecha = new Date(año, mes, dia).getTime();
    return fecha;
  }
  calcularFecha(num) {
    let val: number = +num.target.value;
    if (this.date1 && this.date2) {
      this.date1 = val;
      this.date2 = null;
    } else if (!this.date1) {
      this.date1 = val;
    } else {
      this.date2 = val;
    }
    if (this.date2 > this.date1) {
      let res: number = this.date2 - this.date1;
      res = res / 1000 / 60 / 60 / 24;
      console.log(res);
    }
  }
  reset() {
    this.date1 = null;
    this.date2 = null;
    console.log(this.date1, this.date2);
  }
  siguiente(año, mes) {
    let month = mes + 1;
    if (month < 12) {
      this.month = month;
      this.days = new Date(año, this.month + 1, 0).getDate();
      this.week = new Date(año, this.month, 1).getDay();
    } else {
      this.month = 0;
      this.year = año + 1;
      this.days = new Date(this.year, this.month + 1, 0).getDate();
      this.week = new Date(this.year, this.month, 1).getDay();
    }
    this.nextMonth(año, this.month);
  }
  anterior(año, mes) {
    if (mes > 0) {
      this.month = mes - 1;
      this.days = new Date(año, this.month + 1, 0).getDate();
      this.week = new Date(año, this.month, 1).getDay();
    } else {
      this.month = 11;
      this.year = año - 1;
      this.days = new Date(this.year, this.month + 1, 0).getDate();
      this.week = new Date(this.year, this.month, 1).getDay();
    }
    this.nextMonth(año, this.month);
  }
}
