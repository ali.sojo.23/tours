import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CalendarComponent } from './component/calendar/calendar.component';
import { CalendarMobileComponent } from './component/calendar-mobile/calendar-mobile.component';

@NgModule({
  declarations: [
    AppComponent,
    CalendarComponent,
    CalendarMobileComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
